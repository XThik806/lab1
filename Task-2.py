#Обчислити дробову частину середнього геометричного трьох заданих позитивних чисел.

import math

a = float(input("Введи перше число: "))
b = float(input("Введи друге число: "))
c = float(input("Введи третє число: "))

geometric_mean = math.sqrt(a * b * c)

fractional_part = geometric_mean - int(geometric_mean)

print(f"Середнє геометричне: {geometric_mean:.2f}")
print(f"Дробова частина: {fractional_part:.2f}")